//
// ToggleFavoriteCoin.swift
//
// Generated by swagger-codegen
// https://github.com/swagger-api/swagger-codegen
//

import Foundation



public struct ToggleFavoriteCoin: Codable {

    public var coinId: UUID?
    public var isFavorite: Bool?

    public init(coinId: UUID?, isFavorite: Bool?) {
        self.coinId = coinId
        self.isFavorite = isFavorite
    }


}

