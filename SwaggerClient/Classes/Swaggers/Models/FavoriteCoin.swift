//
// FavoriteCoin.swift
//
// Generated by swagger-codegen
// https://github.com/swagger-api/swagger-codegen
//

import Foundation



public struct FavoriteCoin: Codable {

    public var coinId: UUID?

    public init(coinId: UUID?) {
        self.coinId = coinId
    }


}

